﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Soundscape.Data.Sql.DAO;
using Soundscape.Data.Sql.DAOConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Soundscape.Data.Sql
{
    public class SoundscapeDbContext : DbContext
    {
        public SoundscapeDbContext(DbContextOptions<SoundscapeDbContext> options) : base(options) { }

        //Ustawienie klas z folderu DAO jako tabele bazy danych
        public virtual DbSet<Album> Album { get; set; }
        public virtual DbSet<Artist> Artist { get; set; }
        public virtual DbSet<Song> Song { get; set; }
        public virtual DbSet<Genre> Genre  { get; set; }
        public virtual DbSet<ArtistAlbum> ArtistAlbum { get; set; }
        public virtual DbSet<SongArtist> SongArtist { get; set; }
        public virtual DbSet<SongGenre> SongGenre { get; set; }

        //Przykład konfiguracji modeli/encji poprzez klasy konfiguracyjne z folderu DAOConfigurations
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new AlbumConfiguration());
            builder.ApplyConfiguration(new ArtistConfiguration());
            builder.ApplyConfiguration(new SongConfiguration());
            builder.ApplyConfiguration(new GenreConfiguration());
            builder.ApplyConfiguration(new ArtistAlbumConfiguration());
            builder.ApplyConfiguration(new SongArtistConfiguration());
            builder.ApplyConfiguration(new SongGenreConfiguration());
        }

    }

}
