﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Soundscape.Data.Sql.DAO;


namespace Soundscape.Data.Sql.Migrations
{
    public class DatabaseSeed
    {
        private readonly SoundscapeDbContext _context;

        //wstrzyknięcie instancji klasy FoodlyDbContext poprzez konstruktor
        public DatabaseSeed(SoundscapeDbContext context)
        {
            _context = context;
        }
        public void Seed()
        {
            //add Artists
            var artistList = BuildArtistList();
            _context.Artist.AddRange(artistList);
            _context.SaveChanges();

            //add Albums
            var albumList = BuildAlbumList();
            _context.Album.AddRange(albumList);
            _context.SaveChanges();

            //add Songs
            var songList = BuildSongList();
            _context.Song.AddRange(songList);
            _context.SaveChanges();

            //add Genres
            var genreList = BuildGenreList();
            _context.Genre.AddRange(genreList);
            _context.SaveChanges();

            //add ArtistAlbum
            var artistAlbumList = BuildArtistAlbumList();
            _context.ArtistAlbum.AddRange(artistAlbumList);
            _context.SaveChanges();

            ////add SongArtist
            var songArtistList = BuildSongArtistList();
            _context.SongArtist.AddRange(songArtistList);
            _context.SaveChanges();

            ////add SongGenre
            var songGenreList = BuildSongGenreList();
            _context.SongGenre.AddRange(songGenreList);
            _context.SaveChanges();

        }

        private IEnumerable<DAO.Artist> BuildArtistList()
        {
            var artistList = new List<DAO.Artist>();
            var artist = new DAO.Artist()
            {
                ArtistFirstName = "Grace",
                ArtistLastName = "Smith",
                ArtistNickName = "",
                ArtistBirthDate = new DateTime(1999, 09, 15)
            };
            artistList.Add(artist);

            var artist2 = new DAO.Artist()
            {
                ArtistFirstName = "Aaron",
                ArtistLastName = "Addams",
                ArtistNickName = "ADD",
                ArtistBirthDate = new DateTime(2001, 03, 06)
            };
            artistList.Add(artist2);

            var artist3 = new DAO.Artist()
            {
                ArtistFirstName = "Natalie",
                ArtistLastName = "Graham",
                ArtistNickName = "",
                ArtistBirthDate = new DateTime(1994, 01, 31)
            };
            artistList.Add(artist3);

            return artistList;
        }

        private IEnumerable<DAO.Album> BuildAlbumList()
        {
            var albumList = new List<DAO.Album>();
            var album = new DAO.Album()
            {
                AlbumName = "Anti-sound",
                AlbumReleaseDate = new DateTime(2021, 05, 04),
                AlbumSongNumber = 12
            };
            albumList.Add(album);

            var album2 = new DAO.Album()
            {
                AlbumName = "Leftovers",
                AlbumReleaseDate = new DateTime(2022, 12, 29),
                AlbumSongNumber = 17
            };
            albumList.Add(album2);

            var album3 = new DAO.Album()
            {
                AlbumName = "Sonic Invansion",
                AlbumReleaseDate = new DateTime(2023, 02, 02),
                AlbumSongNumber = 13
            };
            albumList.Add(album3);

            return albumList;
        }


        private IEnumerable<DAO.Song> BuildSongList()
        {
            var songList = new List<DAO.Song>();
            var song = new DAO.Song()
            {
                SongName = "War zone",
                SongLength = "3:43",
                AlbumId = 1
            };
            songList.Add(song);

            var song2 = new DAO.Song()
            {
                SongName = "Painkiller",
                SongLength = "2:32",
                AlbumId = 2
            };
            songList.Add(song2);

            var song3 = new DAO.Song()
            {
                SongName = "Fantasy",
                SongLength = "5:14",
                AlbumId = 3
            };
            songList.Add(song3);

            return songList;
        }

        private IEnumerable<DAO.Genre> BuildGenreList()
        {
            var genreList = new List<DAO.Genre>();
            var genre = new DAO.Genre()
            {
                GenreName = "pop",
            };
            genreList.Add(genre);

            var genre2 = new DAO.Genre()
            {
                GenreName = "hip-hop",
            };
            genreList.Add(genre2);

            var genre3 = new DAO.Genre()
            {
                GenreName = "R&B",
            };
            genreList.Add(genre3);

            return genreList;
        }
        private IEnumerable<DAO.ArtistAlbum> BuildArtistAlbumList()
        {
            var artistAlbumList = new List<DAO.ArtistAlbum>();
            var artistalbum = new DAO.ArtistAlbum()
            {
                ArtistId = 1,
                AlbumId = 1,
            };
            artistAlbumList.Add(artistalbum);

            var artistalbum2 = new DAO.ArtistAlbum()
            {
                ArtistId = 2,
                AlbumId = 2,
            };
            artistAlbumList.Add(artistalbum2);

            var artistalbum3 = new DAO.ArtistAlbum()
            {
                ArtistId = 3,
                AlbumId = 3,
            };
            artistAlbumList.Add(artistalbum3);

            return artistAlbumList;
        }

        private IEnumerable<DAO.SongArtist> BuildSongArtistList()
        {
            var songArtistList = new List<DAO.SongArtist>();
            var songartist = new DAO.SongArtist()
            {
                SongId = 1,
                ArtistId = 1,
            };
            songArtistList.Add(songartist);

            var songartist2 = new DAO.SongArtist()
            {
                SongId = 2,
                ArtistId = 2,
            };
            songArtistList.Add(songartist2);

            var songartist3 = new DAO.SongArtist()
            {
                SongId = 3,
                ArtistId = 3,
            };
            songArtistList.Add(songartist3);

            return songArtistList;
        }

        private IEnumerable<DAO.SongGenre> BuildSongGenreList()
        {
            var songGenreList = new List<DAO.SongGenre>();
            var songgenre = new DAO.SongGenre()
            {
                SongId = 1,
                GenreId = 1,
            };
            songGenreList.Add(songgenre);

            var songgenre2 = new DAO.SongGenre()
            {
                SongId = 2,
                GenreId = 2,
            };
            songGenreList.Add(songgenre2);

            var songgenre3 = new DAO.SongGenre()
            {
                SongId = 3,
                GenreId = 3,
            };
            songGenreList.Add(songgenre3);

            return songGenreList;
        }
    }
}

