﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soundscape.Data.Sql.DAO
{
    public class Genre
    {
        public Genre()
        {
            SongGenreKeys = new List<SongGenre>();
        }

        public int GenreId { get; set; }
        public string GenreName { get; set; }
        
        public virtual ICollection<SongGenre> SongGenreKeys { get; set; }
    }
}
