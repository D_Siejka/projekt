﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soundscape.Data.Sql.DAO
{
    public class SongGenre
    {
        public int SongGenreId { get; set; }
        public int SongId { get; set; }
        public int GenreId { get; set; }

        public virtual Song Song { get; set; }
        public virtual Genre Genre { get; set; }

    }
}
