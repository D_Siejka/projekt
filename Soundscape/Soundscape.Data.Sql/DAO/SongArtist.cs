﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soundscape.Data.Sql.DAO
{
    public class SongArtist
    {
        public int SongArtistId { get; set; }
        public int SongId { get; set; }
        public int ArtistId { get; set; }

        public virtual Song Song { get; set; }
        public virtual Artist Artist { get; set; }

    }
}
