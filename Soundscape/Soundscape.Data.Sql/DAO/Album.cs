﻿using Google.Protobuf.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Soundscape.Data.Sql.DAO
{
    public class Album
    {
        public Album()
        {
            ArtistAlbumKeys = new List<ArtistAlbum>();
            Songs = new List<Song>();
        }

        public int AlbumId { get; set; }
        public string AlbumName { get; set; }
        public DateTime AlbumReleaseDate { get; set; }
        public int AlbumSongNumber { get; set; }

        public virtual ICollection<ArtistAlbum> ArtistAlbumKeys { get; set; }
        public virtual ICollection<Song> Songs { get; set; }
    }
}
