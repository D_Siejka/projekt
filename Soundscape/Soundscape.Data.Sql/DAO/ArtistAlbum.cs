﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soundscape.Data.Sql.DAO
{
    public class ArtistAlbum
    {
        public int ArtistAlbumId { get; set; }
        public int ArtistId { get; set; }
        public int AlbumId { get; set; }

        public virtual Artist Artist { get; set; }
        public virtual Album Album { get; set; }

    }
}
