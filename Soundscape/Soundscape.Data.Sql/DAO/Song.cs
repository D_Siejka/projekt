﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soundscape.Data.Sql.DAO
{
    public class Song
    {
        public Song()
        {
            SongGenreKeys = new List<SongGenre>();
            SongArtistKeys = new List<SongArtist>();
        }

        public int SongId { get; set; }
        public string SongName { get; set; }
        public string SongLength { get; set; }
        public int AlbumId { get; set;}
        
        public virtual Album Album { get; set; }

        public virtual ICollection<SongGenre> SongGenreKeys { get; set; }
        public virtual ICollection<SongArtist> SongArtistKeys { get; set; }
    }
}
