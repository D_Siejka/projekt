﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soundscape.Data.Sql.DAO
{
    public class Artist
    {
        public Artist()
        {
            ArtistAlbumKeys = new List<ArtistAlbum>();
            SongArtistKeys = new List<SongArtist>();
        }

        public int ArtistId { get; set; }
        public string ArtistFirstName { get; set; }
        public string ArtistLastName { get; set; }
        public string ArtistNickName { get; set; }
        public DateTime ArtistBirthDate { get; set; }

        public virtual ICollection<ArtistAlbum> ArtistAlbumKeys { get; set; }
        public virtual ICollection<SongArtist> SongArtistKeys { get; set; }
    }
}
