﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Soundscape.Data.Sql.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soundscape.Data.Sql.DAOConfigurations
{
    public class ArtistAlbumConfiguration : IEntityTypeConfiguration<ArtistAlbum>
    {
        public void Configure(EntityTypeBuilder<ArtistAlbum> builder)
        {
            builder.HasOne(x => x.Album)
               .WithMany(x => x.ArtistAlbumKeys)
               .OnDelete(DeleteBehavior.Restrict)
               .HasForeignKey(x => x.AlbumId);
            builder.HasOne(x => x.Artist)
              .WithMany(x => x.ArtistAlbumKeys)
              .OnDelete(DeleteBehavior.Restrict)
              .HasForeignKey(x => x.ArtistId);
            builder.ToTable("ArtistAlbum");

        }

    }
}
