﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Soundscape.Data.Sql.DAO;

namespace Soundscape.Data.Sql.DAOConfigurations
{
    public class SongConfiguration : IEntityTypeConfiguration<Song>
    {
        public void Configure(EntityTypeBuilder<Song> builder)
        {
            builder.Property(c => c.SongName).IsRequired();
            builder.Property(c => c.SongLength).IsRequired();
            builder.HasOne(x => x.Album)
                .WithMany(x => x.Songs)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.AlbumId);
            builder.ToTable("Song");
        }

    }
}
