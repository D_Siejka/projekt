﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Soundscape.Data.Sql.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soundscape.Data.Sql.DAOConfigurations
{
    public class SongGenreConfiguration : IEntityTypeConfiguration<SongGenre>
    {
        public void Configure(EntityTypeBuilder<SongGenre> builder)
        {
            builder.HasOne(x => x.Song)
               .WithMany(x => x.SongGenreKeys)
               .OnDelete(DeleteBehavior.Restrict)
               .HasForeignKey(x => x.SongId);
            builder.HasOne(x => x.Genre)
              .WithMany(x => x.SongGenreKeys)
              .OnDelete(DeleteBehavior.Restrict)
              .HasForeignKey(x => x.SongId);
            builder.ToTable("SongGenre");

        }
    }
}
