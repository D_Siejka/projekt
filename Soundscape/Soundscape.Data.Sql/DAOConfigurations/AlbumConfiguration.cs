﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Soundscape.Data.Sql.DAO;

namespace Soundscape.Data.Sql.DAOConfigurations
{
    public class AlbumConfiguration : IEntityTypeConfiguration<Album>
    {
        public void Configure(EntityTypeBuilder<Album> builder)
        {
            builder.Property(c => c.AlbumName).IsRequired();
            builder.Property(c => c.AlbumReleaseDate).IsRequired();
            builder.Property(c => c.AlbumSongNumber).IsRequired();
            builder.ToTable("Album");
        }

    }
}
