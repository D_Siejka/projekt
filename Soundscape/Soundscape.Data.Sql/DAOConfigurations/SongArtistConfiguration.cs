﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Soundscape.Data.Sql.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soundscape.Data.Sql.DAOConfigurations
{
    public class SongArtistConfiguration : IEntityTypeConfiguration<SongArtist>
    {
        public void Configure(EntityTypeBuilder<SongArtist> builder)
        {
            builder.HasOne(x => x.Song)
               .WithMany(x => x.SongArtistKeys)
               .OnDelete(DeleteBehavior.Restrict)
               .HasForeignKey(x => x.SongId);
            builder.HasOne(x => x.Artist)
              .WithMany(x => x.SongArtistKeys)
              .OnDelete(DeleteBehavior.Restrict)
              .HasForeignKey(x => x.ArtistId);
            builder.ToTable("SongArtist");

        }
    }
}
