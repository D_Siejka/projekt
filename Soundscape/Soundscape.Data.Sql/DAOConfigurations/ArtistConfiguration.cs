﻿using Microsoft.EntityFrameworkCore;
using Soundscape.Data.Sql.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Soundscape.Data.Sql.DAOConfigurations
{
    public class ArtistConfiguration : IEntityTypeConfiguration<Artist>
    {
        public void Configure(EntityTypeBuilder<Artist> builder)
        {
            builder.Property(c => c.ArtistFirstName).IsRequired();
            builder.Property(c => c.ArtistLastName).IsRequired();
            builder.Property(c => c.ArtistBirthDate).IsRequired();
            builder.ToTable("Artist");
        }

    }
}
