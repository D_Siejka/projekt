﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Soundscape.API.HealthChecks;
using Soundscape.Data.Sql;
using Soundscape.Data.Sql.Migrations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;

namespace Soundscape.API
{

    public class Startup
    {
        //Reprezentuje zestaw właściwości konfiguracyjnych aplikacji klucz / wartość. (np z pliku appsettings.json)
        public IConfiguration Configuration { get; }
        private const string MySqlHealthCheckName = "mysql";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            //rejestracja DbContextu, użycie providera MySQL i pobranie danych o bazie z appsettings.json
            services.AddDbContext<SoundscapeDbContext>(options => options
                .UseMySQL(Configuration.GetConnectionString("SoundscapeDbContext")));
            services.AddTransient<DatabaseSeed>();

            //dodanie health checku i konfiguracja health checku dla MySqla
            services.AddHealthChecks()
                .AddMySql(
                    Configuration.GetConnectionString("SoundscapeDbServer"),
                    5,
                    10,
                    MySqlHealthCheckName);

        }


        // Metoda w której konfiguruje się pipeline (potok) żądań HTTP.
        //Ja użyłem tej metody do upewnienia się, że baza którą chce utworzyć nie istnieje,
        //a następnie do utworzenia bazy danych i utworzenia testowych danych

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //wystawienie pod adresem /healthy stanu healthchecków
            app.UseHealthChecks("/healthy");

            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<SoundscapeDbContext>();
                var databaseSeed = serviceScope.ServiceProvider.GetRequiredService<DatabaseSeed>();
                
                    //sprawdzenie czy health check się powiódł
                var healthCheck = serviceScope.ServiceProvider.GetRequiredService<HealthCheckService>();
                if (healthCheck.CheckHealthAsync().Result?.Entries[MySqlHealthCheckName].Status
                    == HealthCheckResult.Healthy().Status)

                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
                databaseSeed.Seed();
            }
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context => { await context.Response.WriteAsync("Hello World!"); });
            });

        }
    }
}
