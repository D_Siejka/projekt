﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Foodly.Data.Sql;
using Foodly.Data.Sql.Migrations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;


namespace Soundscape.API
{

    public class Startup
    {
        //Reprezentuje zestaw właściwości konfiguracyjnych aplikacji klucz / wartość. (np z pliku appsettings.json)
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            //rejestracja DbContextu, użycie providera MySQL i pobranie danych o bazie z appsettings.json
            services.AddDbContext<SoundscapeDbContext>(options => options
                .UseMySQL(Configuration.GetConnectionString("SoundscapeDbContext")));
            services.AddTransient<DatabaseSeed>();
        }


        // Metoda w której konfiguruje się pipeline (potok) żądań HTTP.
        //Ja użyłem tej metody do upewnienia się, że baza którą chce utworzyć nie istnieje,
        //a następnie do utworzenia bazy danych i utworzenia testowych danych

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<SoundscapeDbContext>();
                var databaseSeed = serviceScope.ServiceProvider.GetRequiredService<DatabaseSeed>();
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
                databaseSeed.Seed();
            }

        }
    }
}
